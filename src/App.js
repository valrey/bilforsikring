import './App.css';
import ForsikringComponent from './bilforsikring/ForsikringComponent';

function App() {
  return (
    <ForsikringComponent />
  );
}

export default App;
