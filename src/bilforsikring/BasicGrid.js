import React from "react";
import { Grid } from "@material-ui/core";


const BasicGrid = (props) => {
  return (
    <>
      <Grid item md={1}></Grid>
      <Grid item md={4} xs={12}>
      {props.children}
      </Grid>
      <Grid item md={7}></Grid>
    </>
  );
};

export default BasicGrid;
