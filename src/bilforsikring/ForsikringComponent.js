import React from "react";
import {
  Grid,
  Button,
  TextField,
  Typography,
  MenuItem,
  Box,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import BasicGrid from "./BasicGrid";

const useStyles = makeStyles({
  input: {
    width: "auto",
    display: "block",
    marginTop: "10px",
  },
  btn: {
    borderRadius: "25px",
  },
  btnBlack: {
    borderRadius: "25px",
    marginRight: "5%",
    backgroundColor: "black",
    "&:hover": {
      backgroundColor: "grey",
    },
    "&:active": {
      backgroundColor: "black",
    },
  },
  text: {
    fontSize: "2.2rem",
    fontWeight: "lighter",
  },
  marginDiv: {
    margin: "3% 0%",
  },
  danger: {
    color: "red",
  },
});

const ForsikringContainer = () => {
  const classes = useStyles();
  const bonusArr = [60, 65, 70, 75, 80, 85];
  const [regnr, setRegnr] = React.useState("");
  const [bonus, setBonus] = React.useState("60");
  const [fnummer, setFnummer] = React.useState("");
  const [name, setName] = React.useState("");
  const [surname, setSurname] = React.useState("");
  const [epost, setEpost] = React.useState("");

  const regnrInvalid =
    regnr.trim() !== "" && (regnr.length < 3 || regnr.length > 8);
  const fnummerInvalid = fnummer.trim() !== "" && fnummer.length < 11;
  const nameInvalid = name.trim() !== "" && name.length < 3;
  const surnameInvalid = surname.trim() !== "" && surname.length < 3;

  const regEmail =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const epostInvalid = epost.trim() !== "" && !regEmail.test(epost);

  const reset = () => {
    setEpost("");
    setFnummer("");
    setName("");
    setRegnr("");
    setSurname("");
    setBonus("60");
    setTextHelp(false);
    setPrice(0);
  };

  const [textHelp, setTextHelp] = React.useState(false);
  const [price, setPrice] = React.useState(undefined);

  const calculate = () => {
    const isInvalid =
      regnrInvalid ||
      fnummerInvalid ||
      nameInvalid ||
      surnameInvalid ||
      epostInvalid;
    const isVoid =
      regnr.trim() === "" ||
      fnummer.trim() === "" ||
      name.trim() === "" ||
      surname.trim() === "" ||
      epost.trim() === "";

    if (isInvalid || isVoid) {
      setPrice(0);
      setTextHelp(true);
      return;
    } else {
      const random = (min, max) =>
        Math.floor(Math.random() * (max - min)) + min;
      const price = random(9000, 11000);
      const bonusCut = (100 - bonus / 10) / 100;
      const finalPrice = Math.floor(price * bonusCut);
      setPrice(finalPrice);
    }
    setTextHelp(true);
  };

  return (
    <Grid container spacing={3} className={classes.text}>
      <Grid item md={1}></Grid>
      <Grid item md={8} xs={12}>
        <Typography variant={"h2"}> Kjøp Bilforsikring</Typography>
        <div className={classes.marginDiv}>
          Det er fire forskjellige forsikringer å velge mellom.
          <br />
          Ansvarsforsikring er lovpålagt om kjøretøyet er registrert og skal
          brukes på veien. I tillegg kan du utvide forsikringen avhengig av hvor
          gammel bilen din er og hvordan du bruker den.
        </div>
      </Grid>
      <Grid item md={3}></Grid>
      <BasicGrid>
        <label htmlFor="regnr">Bilens registreringsnummer</label>
        <TextField
          id="regnr"
          variant="outlined"
          onChange={(e) => setRegnr(e.target.value)}
          value={regnr}
          placeholder="E.g AB 12345"
          className={classes.input}
          fullWidth
          error={regnrInvalid}
          helperText={
            regnrInvalid && "Må være lengre enn 3 og kortere enn 6 tegn"
          }
          inputProps={{ maxLength: 9 }}
        />
      </BasicGrid>

      <BasicGrid>
        <label htmlFor="bonus">Din bonus</label>
        <TextField
          id="bonus"
          name="bonus"
          select
          value={bonus}
          onChange={(e) => setBonus(e.target.value)}
          helperText="Du starter med 60% eller velg din bonus"
          variant="outlined"
          className={classes.input}
          fullWidth
        >
          {bonusArr.map((option) => (
            <MenuItem key={option} value={option}>
              {option}%
            </MenuItem>
          ))}
        </TextField>
      </BasicGrid>

      <BasicGrid>
        <label htmlFor="fnummer">Fødselsnummer</label>
        <TextField
          id="fnummer"
          value={fnummer}
          onChange={(e) =>
            e.target.value.length <= 11 && setFnummer(e.target.value)
          }
          variant="outlined"
          placeholder="11 siffer"
          className={classes.input}
          fullWidth
          inputProps={{ maxLength: 11, type: "number" }}
          error={fnummerInvalid}
          helperText={fnummerInvalid && "Fødselsnummer skal være 11 sifre"}
        />
      </BasicGrid>

      <Grid item md={1}></Grid>
      <Grid item md={4} xs={12}>
        <label htmlFor="navn">Fornavn</label>
        <TextField
          id="navn"
          value={name}
          onChange={(e) => setName(e.target.value)}
          variant="outlined"
          className={classes.input}
          fullWidth
          error={nameInvalid}
          helperText={
            nameInvalid && "Navn kan ikke være tomt eller kortere enn 3 tegn"
          }
        />
      </Grid>
      <Grid item md={4} xs={12}>
        <label htmlFor="etternavn">Etternavn</label>
        <TextField
          id="etternavn"
          variant="outlined"
          value={surname}
          onChange={(e) => setSurname(e.target.value)}
          className={classes.input}
          fullWidth
          error={surnameInvalid}
          helperText={
            surnameInvalid &&
            "Etternavn kan ikke være tomt eller kortere enn 3 tegn"
          }
        />
      </Grid>
      <Grid item md={3}></Grid>

      <BasicGrid>
        <label htmlFor="epost">E-post</label>
        <TextField
          id="epost"
          value={epost}
          onChange={(e) => setEpost(e.target.value)}
          variant="outlined"
          placeholder="test@gmail.com"
          className={classes.input}
          fullWidth
          error={epostInvalid}
          helperText={
            epostInvalid &&
            "Skriv en gyldig e-postadresse"
          }
        />
      </BasicGrid>
    
    {
        textHelp &&
        <>
        <Grid item md={1}></Grid>
      <Grid item xs={10}>
        <Box
          component="div"
          visibility={textHelp ? "visible" : "hidden"}
          className={!price ? classes.danger : ""}
        >
          {price
            ? "Din pris: " + price + " kr."
            : "Vennligst rett feilene. Alle feltene er obligatoriske for å beregne prisen"}
        </Box>
      </Grid>
      <Grid item md={1}></Grid>
        </>
    }

      <Grid item md={1}></Grid>
      <Grid item xs={12} md={6} className={classes.marginDiv}>
        <Button
          onClick={calculate}
          size="large"
          variant="contained"
          color="primary"
          className={classes.btnBlack}
        >
          Beregn pris
        </Button>
        <Button
          onClick={reset}
          size="large"
          variant="outlined"
          className={classes.btn}
        >
          Avbryt
        </Button>
      </Grid>
      <Grid item md={5}></Grid>
    </Grid>
  );
};

export default ForsikringContainer;
